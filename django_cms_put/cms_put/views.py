from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Content

# Create your views here.

def get_contents(request, key):
    keys = Content.objects.values_list('key', flat=True)
    if request.method == "PUT":
        value = request.body.decode('utf-8')
        if key in keys:
            content = Content.objects.get(key=key)
            content.value = value
        else:
            content = Content(key=key, value=value)
        content.save()
        keys = Content.objects.values_list('key', flat=True)
    if key in keys:
        content = Content.objects.get(key=key)
        return HttpResponse(content.value)
    else:
        return HttpResponse("Resource not found for key: " + key)
